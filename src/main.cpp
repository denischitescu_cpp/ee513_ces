#include "DS3231.hpp"


/**
* Starting point of the application
* The path to I2C file will be send as command line argument to the DS3231 class constructor
*/
int main(int argc, char** argv){

	if( argc == 2 ){
		try{
			DS3231 ds_sensor(argv[1]);
			//ds_sensor.set_RTC_time_and_date("Thu", 11, "Mar", 2021, 01, 35, 50, true, false);
			ds_sensor.display_current_RTC_time_and_date();
			//ds_sensor.display_RTC_temperature();

			//ds_sensor.clear_RTC_alarm(true);
			//ds_sensor.set_RTC_alarm_1(11, 57, 55, 2, 5, true, true,false  );
			//ds_sensor.set_RTC_alarm_2(11, 58, 31, 3, false, true, false  );

			//ds_sensor.set_RTC_square_wave(0);
		}
		catch( bus_open_exception& e1 ){
			std::cout << e1.what() << std::endl;
		}
		catch( sensor_connection_exception& e2 ){
			std::cout << e2.what() << std::endl;
		}
		catch( time_date_exception& e3 ){
			std::cout << e3.what() << std::endl;
		}
	}
	else{
		std::cout << "invalid command-line arguments!" << std::endl;
	}

	return 0;
}
