#include "DS3231.hpp"


/**
* @hour - value that represent the desired hour.( 12h or 24h formats )
*
* @hour_reg_addr - If an alarm is to be set, will indicate the address for alarm's hour reg, otherwise RTC hour reg address.
*
* @correction - bit-mask. Useful for setting key-bits when an alarm is to be set.
*
* @format - true = 12h time format , false = 24h time format.
*
* @period - true = P.M. time, false = A.M. time. If @format is false, @period has no effect no metter what value it has.
*
* @err_msg - used when a prefix is needed to easely indentify from which part of code the exception has been thrown from.
*		E.g. when set_RTC_hour(...) is called to set the hour for an alarm and an exception can be thrown by set_RTC_alarm.
*		Default value = empty string.
*/
void DS3231::set_RTC_hour( const BYTE& hour, const BYTE& hour_reg_addr, const BYTE& correction, bool format, bool period, std::string err_msg ) {
	BYTE write_buffer[2];

	if( hour<0 || hour>23 ) {
		throw time_date_exception( err_msg.append("Hour out of bounds: ").append(std::to_string(hour)).
						append(". Accepted range: [0-12] 12h format | [0-23] 24h format") ) ;
	}

	if( format ){
		if( hour > 12 ) {
			throw time_date_exception( err_msg.append("Hour out of bounds for 12h format: ").append(std::to_string(hour)).
							append(". Accepted range: [0-12]") );
		}
		else {
			write_buffer[1] = (decToBcd(hour) | (1<<6 | period<<5)) | correction ; //12h format: key-bits have to be set.( ds3231.hour_reg.[bit7 & bit6] )
										// ds3231.hour_reg.bit6 is set accordingly to @period.
		}
	}
	else{
		write_buffer[1] = decToBcd(hour) | correction; //24h format: key-bits are ok by default.
	}

	write_buffer[0] = hour_reg_addr;
	if( write(this->fd, write_buffer, 2) != 2 ){
		throw time_date_exception( err_msg.append("Failed to write(set) the hour on following reg<addr in decimal>: ").
						append(std::to_string(hour_reg_addr)) );
	}
}


/**
* @minutes - minutes to be set.
*
* @minutes_reg_addr - If an alarm is to be set, will indicate the address for alarm's minutes reg, otherwise RTC minutes reg address.
*
* @correction -  bit-mask. Useful for setting key-bits when an alarm is to be set.
*
* @err_msg - used when a prefix is needed to easely indentify from which part of code the exception has been thrown from.
*		E.g. when set_RTC_hour(...) is called to set the hour for an alarm and an exception can be thrown by set_RTC_alarm.
*		Default value = empty string.
*/
void DS3231::set_RTC_minutes( const BYTE& minutes, const BYTE& minutes_reg_addr, const BYTE& correction, std::string err_msg){
	BYTE write_buffer[2];

	if( minutes < 0 || minutes > 59 ) {
		throw time_date_exception( err_msg.append("Minutes out of bounds: ").append(std::to_string(minutes)).
						append(". Accepted range: [0-59]") ) ;
	}

	write_buffer[0] = minutes_reg_addr;
	write_buffer[1] = decToBcd(minutes) | correction;
	if( write(this->fd, write_buffer, 2) != 2 ){
		throw time_date_exception( std::string("Failed to write(set) the minutes on following reg<addr in decimal>: ").
						append(std::to_string(minutes_reg_addr)) );
	}
}


/**
* @seconds - seconds to be set.
*
* @seconds_reg_addr - If an alarm is to be set, will indicate the address for alarm's seconds reg, otherwise RTC seconds reg address.
*
* @correction -  bit-mask. Useful for setting key-bits when an alarm is to be set.
*
* @err_msg - used when a prefix is needed to easely indentify from which part of code the exception has been thrown from.
*		E.g. when set_RTC_hour(...) is called to set the hour for an alarm and an exception can be thrown by set_RTC_alarm.
*		Default value = empty string.
*/
void DS3231::set_RTC_seconds( const BYTE& seconds, const BYTE& seconds_reg_addr, const BYTE& correction, std::string err_msg ){
	BYTE write_buffer[2];

	if( seconds < 0 || seconds > 59 ) {
		throw time_date_exception( err_msg.append("Seconds out of bounds: ").append(std::to_string(seconds)).
						append(". Accepted range: [0-59]") ) ;
	}

	write_buffer[0] = seconds_reg_addr;
	write_buffer[1] = decToBcd(seconds) | correction;
	if( write(this->fd, write_buffer, 2) != 2 ){
		throw time_date_exception( std::string("Failed to write(set) the seconds on following reg<addr in decimal>: ").
						append(std::to_string(seconds_reg_addr)) );
	}
}


/**
* @week_day - day of the week to be set.(Short form, e.g. Mon,Tue,Wed,Thu,Fri,Sat,Sun)
*
* @day_reg_addr - If an alarm is to be set, will indicate the address for alarm's day reg, otherwise RTC day reg address.
*
* @correction - bit-mask. Useful for setting key-bits when an alarm is to be set.
*/
void DS3231::set_RTC_week_day( const std::string& week_day, const BYTE& day_reg_addr, const BYTE& correction) {
	BYTE write_buff[2];

	if( week_day == "Mon" ){
		write_buff[1] = 1;
	}
	else if( week_day == "Tue" ){
		write_buff[1] = 2;
	}
	else if( week_day == "Wed" ){
		write_buff[1] = 3;
	}
	else if( week_day == "Thu" ){
		write_buff[1] = 4;
	}
	else if( week_day == "Fri" ){
		write_buff[1] = 5;
	}
	else if( week_day == "Sat" ){
		write_buff[1] = 6;
	}
	else if( week_day == "Mon" ){
		write_buff[1] = 7;
	}
	else{
		throw time_date_exception( std::string("Invalid week day: ").
			append(week_day).append(". Accepted days format: \n\tMon - Tue - Wed - Thu - Fri - Sat - Sun") );
	}

	write_buff[0] = day_reg_addr;
	write_buff[1] |= correction;

	if( write(this->fd, write_buff, 2) != 2 ){
		throw time_date_exception( std::string("Failed to write(set) the day on following reg<addr in decimal>: ").
						append(std::to_string(day_reg_addr)) );
	}
}


/**
* @month_date - date of the month to be set.(range: [1, 31])
*
* @mdate_reg_addr - If an alarm is to be set, will indicate the address for alarm's date reg, otherwise RTC date reg address.
*
* @correction - bit-mask. Useful for setting key-bits when an alarm is to be set.
*/
void DS3231::set_RTC_month_date( const BYTE& month_date, const BYTE& mdate_reg_addr, const BYTE& correction, std::string err_msg) {
	BYTE write_buff[2];

	if( month_date<1 || month_date>31 ){
		throw time_date_exception( err_msg.append("Month Date out of bounds: ").append(std::to_string(month_date)).
						append(". The accepted range is [1-31].") );
	}

	write_buff[0] = mdate_reg_addr;
	write_buff[1] = decToBcd(month_date) | correction;
	if( write(this->fd, write_buff, 2) != 2 ){
		throw time_date_exception( std::string("Failed to write(set) the date on following reg<addr in decimal>: ").
						append(std::to_string(mdate_reg_addr)) );
	}
}


/**
* @month - month of the year to be set.(Short form, e.g. Jan, Feb, ..., Dec)
*
* @century - false = 1900+RTC_year_reg, true = 2000+RTC_year_reg. If not specified in method call, by default will be true.
*/
void DS3231::set_RTC_month( const std::string& month, bool century) {
	BYTE write_buff[2];

	if( month == "Jan" ){
		write_buff[1] = 1;
	}
	else if( month == "Feb" ){
		write_buff[1] = 2;
	}
	else if( month == "Mar" ){
		write_buff[1] = 3;
	}
	else if( month == "Apr" ){
		write_buff[1] = 4;
	}
	else if( month == "May" ){
		write_buff[1] = 5;
	}
	else if( month == "Jun" ){
		write_buff[1] = 6;
	}
	else if( month == "Jul" ){
		write_buff[1] = 7;
	}
	else if( month == "Aug" ){
		write_buff[1] = 8;
	}
	else if( month == "Sep" ){
		write_buff[1] = 9;
	}
	else if( month == "Oct" ){
		write_buff[1] = decToBcd(10);
	}
	else if( month == "Nov" ){
		write_buff[1] = decToBcd(11);
	}
	else if( month == "Dec" ){
		write_buff[1] = decToBcd(12);
	}
	else{
		throw time_date_exception( std::string("Invalid month: ").append(month).
			append(". Accepted months format: \n\tJan - Feb - Mar - Apr - May - Jun - Jul - Aug - Sep - Oct - Nov - Dec") );
	}

	write_buff[0] = MONTH_REG_ADDR; //from ds3231_registers_addr enum
	write_buff[1] |= (century << 7); //setting century key-bit
	if( write(this->fd, write_buff, 2) != 2 ){
		throw time_date_exception("Failed to set the month on ds3231 month reg!");
	}
}


/**
* @year - year to be set(range: [1900, 2099]).
*/
void DS3231::set_RTC_year( const unsigned short int& year ) {
	BYTE write_buff[2];

	if( year<1900 || year>2099 ){
		throw time_date_exception(std::string("Year out of bounds: ").append(std::to_string(year)).
			append(". The accepted range is [1900-2099].") );
	}

	write_buff[0] = YEAR_REG_ADDR; //from ds3231_registers_addr enum
	write_buff[1] = decToBcd(year%100);
	if( write(this->fd, write_buff, 2) != 2 ){
		throw time_date_exception("Failed to set the year on ds3231 year reg!");
	}
}


/**
* @alarm_1 - true = set ds3231.control_reg.A1IE_bit = 1, false = set ds3231.control_reg.A2IE_bit = 1, if not already enabled.
*
* @enable - true = set ds3231.control_reg.INTCN_bit = 1, if not already enabled,
*		   and allows A1IE or A2IE bit to be enabled as well, based on @alarm_1 value.
*	    false = only disable(set to 0) A1IE or A2IE based on @alarm_1.
*	    Default value: true.
*/
void DS3231::enable_RTC_alarm(bool alarm_1, bool enable){

	BYTE write_buffer[1] = {CONTROL_REG_ADDR};
	BYTE read_buffer[2]; //it keeps the content of ds32321.control_reg.

	if( write(this->fd, write_buffer, 1) != 1 ){
		throw bus_write_exception("Failed to reset the address for reading the content of control register(located at 0x0E addres)!");
	}

	if( read(this->fd, read_buffer, 1) != 1 ){
		throw bus_read_exception("Failed to read the content of the control register register(located 0x0E address)!");
	}

	if( enable ){
		if( alarm_1 ){
			read_buffer[1] = read_buffer[0] | 0x05;
		}
		else{
			read_buffer[1] = read_buffer[0] | 0x06;
		}
	}
	else{
		if( alarm_1 ){
			read_buffer[1] &= read_buffer[0] & 0xFE;
		}
		else{
			read_buffer[1] &= read_buffer[0] & 0xFD;
		}
	}

	read_buffer[0] = CONTROL_REG_ADDR;
	if( write(this->fd, read_buffer, 2) != 2 ){
		throw bus_write_exception("Failed to update the content of control register(located at 0x0E addres)!");
	}
}


/**
* Reset A1F or A2F ds3231.status_reg alarm flags
*
* @alarm_1 - true = ds3231.status_register.A1F_bit = 0, false = ds3231.status_register.A2F_bit = 0.
*/
void DS3231::trigger_RTC_alarm( bool alarm_1 ){
	BYTE write_buffer[1] = {STATUS_REG_ADDR};
	BYTE read_buffer[2]; //it keeps the content of ds32321.status_reg.

	if( write(this->fd, write_buffer, 1) != 1 ){
		throw bus_write_exception("Failed to reset the address for reading the content of status register(located at 0x0F addres)!");
	}

	if( read(this->fd, read_buffer, 1) != 1 ){
		throw bus_read_exception("Failed to read the content of the status register(located 0x0F address)!");
	}

	if( alarm_1 ){
		read_buffer[1] = read_buffer[0] & 0xFE;
	}
	else{
		read_buffer[1] = read_buffer[0] & 0xFD;
	}

	read_buffer[0] = STATUS_REG_ADDR;
	if( write(this->fd, read_buffer, 2) != 2 ){
		throw bus_write_exception("Failed to update the content of status register(located at 0x0F addres)!");
	}
}



