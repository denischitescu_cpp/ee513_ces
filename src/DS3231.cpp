#include "DS3231.hpp"


/**
*---------------------------------
* array used to display the DAY according to the value in day register(03h) of DS3231 RTC chip
*---------------------------------
*/
const char* DAYS[7] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };


/**
*---------------------------------
* array used to display the MONTH according to the value in month/century register(05h) of DS3231 RTC chip
*---------------------------------
*/
const char* MONTHS[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };



/**
*--------------------------------
* Helper function - used for bcd-to-decimal conversion.
*--------------------------------
*/
int bcdToDec( unsigned char b ){ return (b >> 4)*10 + (b & 0xF); }


/**
*--------------------------------
* Helper function - used for decimal-to-bcd conversion.
*--------------------------------
*/
unsigned char decToBcd( unsigned char d ){ return ((d/10)<<4) | ((d%10) & 0x0f); }



/**
*----------------------------------------------------------------------------------------------------
* DS3231 definition
*----------------------------------------------------------------------------------------------------
*/
/**
* @bus_file_name - path to the i2c file
*/
DS3231::DS3231(const char* bus_file_name){
	std::cout << "Creating DS3231 object..." << std::endl;

	if( (this->fd = open(bus_file_name, O_RDWR)) < 0)
		throw bus_open_exception();

	if( ioctl(this->fd, I2C_SLAVE, 0x68) < 0 )
		throw sensor_connection_exception();

	std::cout << "\t DS3231 object successfully created:" << std::endl;
	std::cout << "\t\t 1. I2C bus successfully opened." << std::endl;
	std::cout << "\t\t 2. Connection with sensor successfully established." << std::endl;
}

DS3231::~DS3231(){
	std::cout << "Destroying DS3231 object..." << std::endl;
	close(this->fd);
	std::cout << "\t 1. I2C bus properly closed." << std::endl;
	std::cout << "\t 2. Destructor was called." << std::endl;
}


/**
* Read the time and date registers(0x00 - 0x06) of DS3231 RTC module and process their values
*/
void DS3231::display_current_RTC_time_and_date(){
	unsigned char write_buffer[1] = {SECONDS_REG_ADDR};

	if( write(this->fd, write_buffer, 1) != 1 ){
		throw bus_write_exception("Failed to reset the read address for time registers!");
	}

	unsigned char read_buffer[7];
	if( read(this->fd, read_buffer, 7) != 7 ){
		throw bus_read_exception("Failed to read time registers!");
	}

	//----------------------------------------------------------


	/**
	* Displaying the day as a string - based on DAYS array.
	* Accepted range(ds3231 data sheet): [1, 7]. A range check is performed.
	*/
	if( (read_buffer[3] & 0x07) == 0x00 ){
		throw time_date_exception("Day range: [1, 7]. Actual value: 0");
	}
	else{
		printf("%s - ", DAYS[ (read_buffer[3] & 0x07 ) - 1 ] );
	}


	/*
	* Displaying the date(no. of the day of the month) as an integer.
	* Accepted range(ds3231 data sheet): [01, 31]. A range check is performed.
	*/
	if( (bcdToDec(read_buffer[4] & 0x3f) >= 1) && (bcdToDec(read_buffer[4] & 0x3f) <= 31) ){
		printf("%02d/", bcdToDec(read_buffer[4] & 0x3f));
	}
	else{
		throw time_date_exception(std::string("Date range: [1, 31]. Actual value: ").append(std::to_string(bcdToDec(read_buffer[4] &  0x3f))) );
	}


	/**
	* Displaying the month as a string - based on MONTH array.
	* Accepted range(ds3231 data sheet): [01, 12]. A range check is performed.
	*/
	if( (bcdToDec(read_buffer[5] & 0x1f) >= 1) && (bcdToDec(read_buffer[5] & 0x1f) <= 12) ){
		printf("%s/", MONTHS[ bcdToDec(read_buffer[5] & 0x1f) - 1 ] );
	}
	else{
		throw time_date_exception(std::string("Month range: [1,12]. Actual value: ").append(std::to_string(bcdToDec(read_buffer[5] & 0x1f))) );
	}


	/**
	* Displaying the year as an integer.
	* Accepted range(ds3231): [0, 99]. A range check is performed.
	*/
	if( (bcdToDec(read_buffer[6]) >= 0) && (bcdToDec(read_buffer[6]) <= 99) ){
		if( (read_buffer[5] & 0x80) == 0x80 ) { // century bit = 1 => 2000 + year
			printf("%d - ", 2000 + bcdToDec(read_buffer[6]) );
		}
		else{ //century bit = 0 => 1900 + year
			printf("%d - ", 1900 + bcdToDec(read_buffer[6]) );
		}
	}
	else{
		throw time_date_exception(std::string("Year range: [0, 99]. Actual value: ").append(std::to_string(bcdToDec(read_buffer[6]))) );
	}



	/**
	* Displaying time in HH:MM:SS format.
	*/
	if( (read_buffer[2] & 0x40) == 0x40 ) { //if true, time is in 12-hours format, otherwise is in 24-hours format
		if( (read_buffer[2] & 0x20) == 0x20 ) { // if true, means p.m. hour otherwise a.m. hour
			printf("%02d:%02d:%02d P.M.\n", bcdToDec(read_buffer[2] & 0x1F), bcdToDec(read_buffer[1]), bcdToDec(read_buffer[0]));
		}
		else{
			printf("%02d:%02d:%02d A.M.\n", bcdToDec(read_buffer[2] & 0x1F), bcdToDec(read_buffer[1]), bcdToDec(read_buffer[0]));
		}
	}
	else{
		printf("%02d:%02d:%02d [24h format]\n", bcdToDec(read_buffer[2] & 0x3F), bcdToDec(read_buffer[1]), bcdToDec(read_buffer[0]));
	}

	//----------------------------------------------------------
}


/**
* Read temperature registers(0x11 - 0x12) of DS3231 RTC module and process their values.
*/
void DS3231::display_RTC_temperature(){
	unsigned char read_addr = 0x11;
	unsigned char write_buffer[1];
	write_buffer[0] = read_addr;

	if( write(this->fd, write_buffer, 1) != 1 ){
		throw bus_write_exception("Failed to reset the read address for temperature registers!");
	}

	char read_buffer[2];
	if( read(this->fd, read_buffer, 2) != 2 ){
		throw bus_read_exception("Failed to read temperature registers!");
	}

	printf("current temp is: %d.%d\n", read_buffer[0], (read_buffer[1]>>6)*25 );
}


/**
* called methods are described in DS3231_private_interface.cpp
*
* @format - true = 12h time format , false = 24h time format. Default value: false.
*
* @period - true = P.M. time, false = A.M. time. If @format is false, @period has no effect no metter what value it has.
*		Default value: false.
*/
void DS3231::set_RTC_time_and_date(std::string week_day,  BYTE month_date, std::string month, unsigned short int year,
						BYTE hour, BYTE minutes, BYTE seconds, bool format, bool period){

	set_RTC_hour(hour, HOUR_REG_ADDR, 0x00, format, period);
	set_RTC_minutes(minutes, MINUTES_REG_ADDR, 0x00);
	set_RTC_seconds(seconds, SECONDS_REG_ADDR, 0x00);

	set_RTC_week_day(week_day, DAY_REG_ADDR, 0x00) ;
	set_RTC_month_date(month_date, DATE_REG_ADDR, 0x00) ;
	set_RTC_month(month, (year/100) == 20 ) ;
	set_RTC_year( year ) ;

	std::cout << "Time and date has been set successfully!" << std::endl;
}




/**
* @day - true = alarm will be the result of a match with day of the week, false = alarm will be the result of a match with the date of the month.
*		Default value: true.
*
* @day_date - if @day=true, day_date will indicate the day of the week(range: [1, 7]).
*		If @day=false, day_date will indicate the date of the month(range: [1, 31]).
*
* @format - true = alarm 1 time in 12h format , false = alarm 1 time in 24h format. Default value: false.
*
* @period - true = alarm 1 time as P.M. time, false = alarm 1 time as A.M. time. If @format is false, @period has no effect no metter what value it has.
*		Default value: false.
*
* @mode - will determine how MASK BITS(bit 7) will be set. Range: [0, 5]. Default value mode = 5.
*	mode = 0 : alarm once per second, @day is ignored.
*	mode = 1 : alarm when seconds match, @day is ignored.
*	mode = 2 : alarm when minutes and seconds match, @day is ignored.
*	mode = 3 : alarm when hours, minutes and seconds match, @day is ignored.
*	mode = 4 : alarm when date, hours, minutes and seconds match.
*	mode = 5 : alarm when day, hours, minutes and seconds match.
*/
void DS3231::set_RTC_alarm_1( BYTE hour, BYTE minutes, BYTE seconds, BYTE day_date, BYTE mode, bool day, bool format, bool period ){ 

	BYTE alarm_mask[4]; //value for ds3231.alarm1_[seconds|minutes|hours|day-date].bit_7 .
			    // alarm_mask[0] - A1M4 | alarm_mask[1] - A1M3 | alarm_mask[2] - A1M2 | alarm_mask[3] - A1M1 .

	switch( mode ){
		case 0:	alarm_mask[0] = 1; alarm_mask[1] = 1; alarm_mask[2] = 1; alarm_mask[3] = 1;
			break;
		case 1:	alarm_mask[0] = 1; alarm_mask[1] = 1; alarm_mask[2] = 1; alarm_mask[3] = 0;
			break;
		case 2:	alarm_mask[0] = 1; alarm_mask[1] = 1; alarm_mask[2] = 0; alarm_mask[3] = 0;
			break;
		case 3: alarm_mask[0] = 1; alarm_mask[1] = 0; alarm_mask[2] = 0; alarm_mask[3] = 0;
			break;
		case 4: alarm_mask[0] = 0; alarm_mask[1] = 0; alarm_mask[2] = 0; alarm_mask[3] = 0;
			break;
		case 5: alarm_mask[0] = 0; alarm_mask[1] = 0; alarm_mask[2] = 0; alarm_mask[3] = 0;
			break;
		default: throw time_date_exception( std::string("ALARM_1_ERR: Invalid alarm mode: ").append(std::to_string(mode)).
				append(". Valid range: [0, 5] .") );
			break;
	}

	set_RTC_seconds( seconds, A1_SECONDS_REG_ADDR, alarm_mask[3]<<7, std::string("ALARM_1_ERR: ") );

	set_RTC_minutes( minutes, A1_MINUTES_REG_ADDR, alarm_mask[2]<<7, std::string("ALARM_1_ERR: ") );

	set_RTC_hour( hour, A1_HOUR_REG_ADDR, alarm_mask[1]<<7, format, period, std::string("ALARM_1_ERR: ")  );

	if( day ){ // DY/DT flag is set by a logical OR with @day value.
		if( day_date < 1 || day_date > 7)
			throw time_date_exception(  std::string("ALARM_1_ERR: Invalid week day index: ").append(std::to_string(day_date)).
					append(". Accepted days index range: \n\t1(Mon) - 2(Tue) - 3(Wed) - 4(Thu) - 5(Fri) - 6(Sat) - 7(Sun)")  );

		set_RTC_week_day( DAYS[day_date-1], A1_DAY_DATE_REG_ADDR, (alarm_mask[0]<<7) | (day << 6) );
	}
	else{
		set_RTC_month_date( day_date, A1_DAY_DATE_REG_ADDR, (alarm_mask[0]<<7) | (day << 6), std::string("ALARM_1_ERR: ") );
	}

	trigger_RTC_alarm(true); //// ds3231.status_reg.A1F = 0

	enable_RTC_alarm(true, true); //enable ds3231.control_reg.[INTCN | A1IE]

	std::cout << "Alarm 1 has been set and enabled!" << std::endl;
}


/**
* @day - true = alarm will be the result of a match with day of the week, false = alarm will be the result of a match with the date of the month.
*		Default value: true.
*
* @day_date - if @day=true, day_date will indicate the day of the week(range: [1, 7]).
*		If @day=false, day_date will indicate the date of the month(range: [1, 31]).
*
* @format - true = alarm 1 time in 12h format , false = alarm 1 time in 24h format. Default value: false.
*
* @period - true = alarm 1 time as P.M. time, false = alarm 1 time as A.M. time. If @format is false, @period has no effect no metter what value it has.
*		Default value: false.
*
* @mode - will determine how MASK BITS(bit 7) will be set. Range: [0, 4]. Default value mode = 3.
*	mode = 0 : alarm once per minute (00 of every minute), @day is ignored.
*	mode = 1 : alarm when minutes match, @day is ignored.
*	mode = 2 : alarm when hours and minutes match, @day is ignored.
*	mode = 3 : alarm when date, hours and minutes match.
*	mode = 4 : alarm when day, hours and minutes match.
*/
void DS3231::set_RTC_alarm_2( BYTE hour, BYTE minutes, BYTE day_date, BYTE mode, bool day, bool format, bool period ){
	BYTE alarm_mask[3]; //value for ds3231.alarm_2_[seconds|minutes|hours|day-date].bit_7 .
			    // alarm_mask[0] - A2M4 | alarm_mask[1] - A2M3 | alarm_mask[2] - A2M2 .

	switch( mode ){
		case 0:	alarm_mask[0] = 1; alarm_mask[1] = 1; alarm_mask[2] = 1;
			break;
		case 1:	alarm_mask[0] = 1; alarm_mask[1] = 1; alarm_mask[2] = 0;
			break;
		case 2:	alarm_mask[0] = 1; alarm_mask[1] = 0; alarm_mask[2] = 0;
			break;
		case 3: alarm_mask[0] = 0; alarm_mask[1] = 0; alarm_mask[2] = 0;
			break;
		case 4: alarm_mask[0] = 0; alarm_mask[1] = 0; alarm_mask[2] = 0;
			break;
		default: throw time_date_exception( std::string("ALARM_2_ERR: Invalid alarm mode: ").append(std::to_string(mode)).
				append(". Valid range: [0, 4] .") );
			break;
	}


	set_RTC_minutes( minutes, A2_MINUTES_REG_ADDR, alarm_mask[2]<<7, std::string("ALARM_2_ERR: ") );

	set_RTC_hour( hour, A2_HOUR_REG_ADDR, alarm_mask[1]<<7, format, period, std::string("ALARM_2_ERR: ")  );

	if( day ){ // DY/DT flag is set by a logical OR with @day value.
		if( day_date < 1 || day_date > 7)
			throw time_date_exception(  std::string("ALARM_2_ERR: Invalid week day index: ").append(std::to_string(day_date)).
					append(". Accepted days index range: \n\t1(Mon) - 2(Tue) - 3(Wed) - 4(Thu) - 5(Fri) - 6(Sat) - 7(Sun)")  );

		set_RTC_week_day( DAYS[day_date-1], A2_DAY_DATE_REG_ADDR, (alarm_mask[0]<<7) | (day << 6) );
	}
	else{
		set_RTC_month_date( day_date, A2_DAY_DATE_REG_ADDR, (alarm_mask[0]<<7) | (day << 6), std::string("ALARM_2_ERR: ") );
	}

	trigger_RTC_alarm(false); //reset alarm 2 flag(ds3231.status_reg.A2F).

	enable_RTC_alarm(false, true); //enable ds3231.control_reg.[INTCN | A2IE]


	std::cout << "Alarm 2 has been set and enabled!" << std::endl;
}




/**
* @mode - select square-wave output frequency. Range: [0, 3]. Default value: mode = 0.
	mode = 0: 1 Hz
	mode = 1: 1024 kHz
	mode = 2: 4096 kHz
	mode = 3: 8192 kHz
*/
void DS3231::set_RTC_square_wave( BYTE mode ){

	BYTE write_buffer[1] = {CONTROL_REG_ADDR};
	BYTE read_buffer[2]; //it keeps the content of ds32321.control_reg.

	BYTE output_freq_rate[2]; //values for ds3231.control_reg.[ RS1 | RS2 ]
				  // output_freq_rate[0] - RS2 | output_freq_rate[1] - RS1

	switch( mode ){
		case 0: output_freq_rate[0] = 0, output_freq_rate[1] = 0;
			break;

		case 1: output_freq_rate[0] = 0, output_freq_rate[1] = 1;
			break;

		case 2: output_freq_rate[0] = 1, output_freq_rate[1] = 0;
			break;

		case 3: output_freq_rate[0] = 1, output_freq_rate[1] = 1;
			break;

		default: throw time_date_exception( std::string("SQW_ERR: Invalid frequency rate mode: ").append(std::to_string(mode)).
				append(". \n\tValid range: 0(1 Hz), 2(1024 kHz), 3(4096 kHz), 4(8192 kHz) .") );
			break;
	}


	if( write(this->fd, write_buffer, 1) != 1 ){
		throw bus_write_exception("Failed to reset the address for reading the content of control register(located at 0x0E addres)!");
	}

	if( read(this->fd, read_buffer, 1) != 1 ){
		throw bus_read_exception("Failed to read the content of the control register(located 0x0E address)!");
	}

	//set INTCN =0 and RS2 = RS1 = 0 to be able set any of other rates. After these, set the rates according to @mode
	read_buffer[1] = (read_buffer[0] & 0xE3) | (output_freq_rate[0] << 4) | (output_freq_rate[1] << 3);

	read_buffer[0] = CONTROL_REG_ADDR;
	if( write(this->fd, read_buffer, 2) != 2 ){
		throw bus_write_exception("Failed to update the content of control register(located at 0x0E addres)!");
	}
}


/**
* Set on 0 both ds3231.control_reg.AxIE and ds3231.status_reg.AxF alarm's flags.
*		 x=1 - alarm 1 flags | x=2 - alarm 2 flags
*
* @alarm_1 - true = ds3231.alarm_1  | false = ds3231.alarm_2
*/
void DS3231::clear_RTC_alarm( bool alarm_1 ){

	if ( alarm_1 ){
		enable_RTC_alarm(true, false); //set ds3231.control_reg.A1IE = 0
		trigger_RTC_alarm(true); //set ds3231.status_reg.A1F = 0
	}
	else{
		enable_RTC_alarm(false, false); //set ds3231.control_reg.A2IE = 0
		trigger_RTC_alarm(false); //set ds3231.status_reg.A2F = 0
	}
}
