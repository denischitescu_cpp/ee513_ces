#ifndef _CUSTOM_EXCEPTIONS_HPP_
#define _CUSTOM_EXCEPTIONS_HPP_


#include<string> // std::string
#include<exception> //std::exception

/**
*---------------------------------------------------------------------------
* bus_open_exception  declaration
*---------------------------------------------------------------------------
*/
struct bus_open_exception : public std::exception {
	const char* what() const noexcept;
};

/**
*---------------------------------------------------------------------------
* sensor_connection__exception declaration
*---------------------------------------------------------------------------
*/
struct sensor_connection_exception : public std::exception {
	const char* what() const noexcept;
};

/**
*---------------------------------------------------------------------------
* bus_write_exception declaration
*---------------------------------------------------------------------------
*/
struct bus_write_exception : public std::exception {
	bus_write_exception(); //default constructor
	bus_write_exception(std::string); //parameterized constructor
	const char* what() const noexcept;
private:
	std::string err_msg;
};


/**
*---------------------------------------------------------------------------
* bus_read_exception declaration
*---------------------------------------------------------------------------
*/
struct bus_read_exception : public std::exception {
	bus_read_exception(); //default constructor
	bus_read_exception(std::string); //parameterized constructor
	const char* what() const noexcept;
private:
	std::string err_msg;
};


/**
*---------------------------------------------------------------------------
* time_date_exception declaration
*---------------------------------------------------------------------------
*/
struct time_date_exception : public std::exception {
	time_date_exception(std::string); //parameterized constructor
	const char* what() const noexcept;
private:
	std::string err_msg;
};


#endif //_CUSTOM_EXCEPTIONS_HPP_

