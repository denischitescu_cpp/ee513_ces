#ifndef _DS3231_HPP_
#define _DS3231_HPP_



#include<iostream> // std::[cout | endl | err]
#include<unistd.h>
#include<fcntl.h> // open(...)
#include<sys/ioctl.h> // ioctl(...)
#include<linux/i2c-dev.h> // I2C_SLAVE

#include "custom_exceptions.hpp" // custom exceptions

typedef unsigned char BYTE;


/**
*---------------------------------------------------------------------------
* DS3231 declaration
*---------------------------------------------------------------------------
*/
class DS3231{
public:
	DS3231(const char*);
	virtual ~DS3231();
	void display_current_RTC_time_and_date();
	void display_RTC_temperature();
	void set_RTC_time_and_date( std::string, BYTE, std::string, unsigned short int, BYTE, BYTE, BYTE,bool format = false, bool period = false);
	void set_RTC_alarm_1( BYTE, BYTE, BYTE, BYTE, BYTE mode = 5, bool day = true, bool format = false, bool period = false );
	void set_RTC_alarm_2( BYTE, BYTE, BYTE, BYTE mode = 3, bool day = true, bool format = false, bool period = false );
	void clear_RTC_alarm( bool alarm_1 = true );
	void set_RTC_square_wave( BYTE mode = 0 );

private:
	int fd;

	//enum used to make the code easier to read
	enum ds3231_registers_addr : BYTE { SECONDS_REG_ADDR=0x00, MINUTES_REG_ADDR, HOUR_REG_ADDR, DAY_REG_ADDR, DATE_REG_ADDR, MONTH_REG_ADDR,
		YEAR_REG_ADDR, A1_SECONDS_REG_ADDR, A1_MINUTES_REG_ADDR, A1_HOUR_REG_ADDR, A1_DAY_DATE_REG_ADDR,  A2_MINUTES_REG_ADDR, A2_HOUR_REG_ADDR,
		A2_DAY_DATE_REG_ADDR, CONTROL_REG_ADDR, STATUS_REG_ADDR };

	void set_RTC_hour( const BYTE&, const BYTE&, const BYTE&, bool, bool, std::string err_msg="" ) ;
	void set_RTC_minutes( const BYTE&, const BYTE&, const BYTE&, std::string err_msg="" ) ;
	void set_RTC_seconds( const BYTE&, const BYTE&, const BYTE&, std::string err_msg="" ) ;
	void set_RTC_week_day( const std::string&, const BYTE&, const BYTE& ) ;
	void set_RTC_month_date( const BYTE&, const BYTE&, const BYTE&, std::string err_msg="" ) ;
	void set_RTC_month( const std::string&, bool century=true) ;
	void set_RTC_year( const unsigned short int& ) ;

	void enable_RTC_alarm(bool, bool enable = true);
	void trigger_RTC_alarm( bool alarm_1 = true );
};


/**
* Helper functions for bcd-to-dec / dec-to-bcd conversions. Are defined in DS3231.cpp 
*/
int bcdToDec( unsigned char );

unsigned char decToBcd( unsigned char d );

#endif
