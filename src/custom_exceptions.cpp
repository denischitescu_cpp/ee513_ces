#include "custom_exceptions.hpp"


/**
*------------------------------------------------------------------------
* bus_open_exception struct definition
*------------------------------------------------------------------------
*/
const char* bus_open_exception::what() const noexcept{
	return "Failed to open the I2C bus!";
}


/**
*------------------------------------------------------------------------
* sensor_connection_exception struct definition
*------------------------------------------------------------------------
*/
const char* sensor_connection_exception::what() const noexcept{
	return "Failed to connect to the sensor!";
}


/**
*------------------------------------------------------------------------
* bus_write_exception struct definition
* @err_msg - allows a short description of the exception
*------------------------------------------------------------------------
*/
bus_write_exception::bus_write_exception() {
	this->err_msg = std::string("Failed to write on I2C bus!");
}

bus_write_exception::bus_write_exception( std::string err_msg ) {
	this->err_msg = std::string("Failed to write on I2C bus: ");
	this->err_msg.append(err_msg);
}

const char* bus_write_exception::what() const noexcept {
	return &this->err_msg[0]; 
}



/**
*------------------------------------------------------------------------
* bus_read_exception struct definition
* @err_msg - allows a short description of the exception
*------------------------------------------------------------------------
*/
bus_read_exception::bus_read_exception() {
	this->err_msg = std::string("Failed to read on I2C bus!");
}

bus_read_exception::bus_read_exception( std::string err_msg ) {
	this->err_msg = std::string("Failed to read on I2C bus: ");
	this->err_msg.append(err_msg);
}

const char* bus_read_exception::what() const noexcept {
	return &this->err_msg[0]; 
}




/**
*------------------------------------------------------------------------
* time_date_exception struct definition
* @err_msg - allows a short description of the exception
*------------------------------------------------------------------------
*/
time_date_exception::time_date_exception( std::string err_msg ) {
	this->err_msg = std::string("Time_Date_Err: ");
	this->err_msg.append(err_msg);
}

const char* time_date_exception::what() const noexcept {
	return &this->err_msg[0];
}
















