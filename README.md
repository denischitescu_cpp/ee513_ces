# EE513_CES

Repository created for EE513-Assignment_1.

The aim of this assignment is to get some hands-on experience interfacing embedded systems to the real world using electronic sensors.
The idea behind is to write high-level software to wrap the low-level interface.

More details can be found in /docs/Assignment_1_EE513_Doc.pdf.
